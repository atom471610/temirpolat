import model.Human;
import service.HumanService;

import java.util.Scanner;

import static service.HumanService.printHuman;

public class Main {

    static Scanner scannerInt = new Scanner(System.in);
    static Scanner scannerStr = new Scanner(System.in);

    public static void main(String[] args) {
        HumanService humanService = new HumanService();

        int step = 5;
        while (step!=0){
            System.out.println("1.Add human. 2.Print human. 3.Delete human. 4.Exit?");
            int a = scannerInt.nextInt();
            switch (a){
                case 1 -> {
                    Human human = createHuman();
                    humanService.addHuman(human);
                }
                case 2 -> {
                    Human[] humans = humanService.getHumans();
                    printHuman(humans);
                }
                case 3 -> {
                    Human[] humans = humanService.getHumans();
                    deleteHumans(humans);
                    System.out.println("66555436354654");
                }
                case 4 -> {
                    System.out.println("Thank you bye !");
                    return;
                }
            }
        }
    }

    public static Human createHuman(){
        Human human = new Human();
        System.out.println("Enter name:");
        human.name = scannerStr.nextLine();
        System.out.println("Enter age:");
        human.age = scannerInt.nextInt();
        System.out.println("Enter job:");
        human.job = scannerStr.nextLine();
        return human;
    }

    public static void deleteHumans(Human humans[]){
        System.out.print("Enter delete name: ");
        String str = scannerStr.nextLine();
        int count = humans.length;
        for (int i = 0; i < count; i++) {
            if (humans[i]!= null && humans[i].name.equals(str)) {
                for (int j = i; j < count-1; j++){
                    humans[j] = humans[j+1];
                }
                count--;
                System.out.println("Delete name successfully.");
                return;
            }
        }
        System.out.println("Name not found.");
        return;
    }
}