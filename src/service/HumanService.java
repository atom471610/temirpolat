package service;

import model.Human;

public class HumanService {

    public Human[] humans = new Human[100000];
    public int index = 0;

    public boolean addHuman(Human human){
        if (!hasHuman(human.name)){
            humans[index++] = human;
            System.out.println("Human added successfully.");
            return true;
        }
        System.out.println("Human exists already?");
        return false;
    }
    public boolean hasHuman(String name){
        for (int i=0;i<index;i++){
            if (humans[i] != null && humans[i].name.equals(name)){
                return true;
            }
        }
        return false;
    }

    public static void printHuman(Human[] humans){
        for (int i=0;i<humans.length;i++){
            if (humans[i] != null){
                System.out.println("Human->" + i + humans[i].toString());
            }
        }
    }

    public Human[] getHumans(){
        return humans;
    }
}
